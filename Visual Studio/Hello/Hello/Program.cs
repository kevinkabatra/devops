﻿using System;

namespace Hello
{
    class Program
    {
        static void  Main(string[] args)
        {
            Console.WriteLine("Hello World");
            string[] persons;

            if (args.Length != 0)
            {
                persons = new string[args.Length];
                persons = args;
            }
            else
            { 
                Console.WriteLine("Please enter your name.");
                persons    = new string[1]     ;
                persons[0] = Console.ReadLine();
            }
            foreach (string person in persons)
            {
                Console.WriteLine(String.Format("And salutations to you as well {0}.", person));
            }
        }
    }
}
