# IMPORTANT: Before releasing this package, copy/paste the next 2 lines into PowerShell to remove all comments from this file:
#   $f='c:\path\to\thisFile.ps1'
#   gc $f | ? {$_ -notmatch "^\s*#"} | % {$_ -replace '(^.*?)\s*?[^``]#.*','$1'} | Out-File $f+".~" -en utf8; mv -fo $f+".~" $f

$ErrorActionPreference = 'Stop'; # stop on all errors
$packageName           = 'hello'
$toolsDir              = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$fileLocation          = Join-Path $toolsDir 'HelloSetup.msi'
$packageArgs           = @{
  packageName   = $packageName
  unzipLocation = $toolsDir
  fileType      = 'MSI'
  file          = $fileLocation
  softwareName  = 'hello*'

  # Checksums are now required as of 0.10.0.
  # To determine checksums, you can get that from the original site if provided.
  # You can also use checksum.exe (choco install checksum) and use it
  # e.g. checksum -t sha256 -f path\to\file
  checksum      = ''
  checksumType  = 'sha256' #default is md5, can also be sha1, sha256 or sha512
  checksum64    = ''
  checksumType64= 'sha256' #default is checksumType

  #MSI
  silentArgs    = "/qn /norestart /l*v `"$($env:TEMP)\$($packageName).$($env:chocolateyPackageVersion).MsiInstall.log`"" # ALLUSERS=1 DISABLEDESKTOPSHORTCUT=1 ADDDESKTOPICON=0 ADDSTARTMENU=0
  validExitCodes= @(0, 3010, 1641)
}

$filepath = "$env:ProgramFiles\Microsoft Dynamics AX\60\ManagementUtilities\LSRetail.First.Dynamics.ManagementUtilities.ps1"
Import-Module $filepath -Force

if(Check-Dependency-AXModel -AXModelName ('SYS Model', 'Foundation') -AXModelVersion ('1.0.0.0', '6.3.164.0'))
{
    Write-Debug 'AX model dependency check passed...'
}
else
{
    throw 'EXCEPTION: AX model dependency check failed...'
}

if(Check-Dependency-Hotfix -Hotfix 'KB3151864')
{
    Write-Debug 'Hotfix dependency check passed...'
}
else
{
    throw 'EXCEPTION: Hotfix dependency check failed...'
}

Install-ChocolateyInstallPackage @packageArgs