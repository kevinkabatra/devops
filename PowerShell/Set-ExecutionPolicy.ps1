<#
.SYNOPSIS
    Resolves error on WIndows 10 Machines: "execution of scripts is disabled on this system."
.NOTES
    Source: http://stackoverflow.com/questions/4037939/powershell-says-execution-of-scripts-is-disabled-on-this-system
#>

Set-ExecutionPolicy RemoteSigned