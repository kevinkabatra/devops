<#
.SYNOPSIS
    This scipt contains methods that will help facilitate automated management of Microsoft Dynamics AX 2012.
.INPUTS
    None. You cannot pipe object to this script.
.OUTPUTS
    None. You cannot pipe this script to another object.
.NOTES
    Contributors: Kevin Kabatra
.EXAMPLE
    Check-Dependency-AXModel -AXModelName ('SYS Model', 'Foundation') -AXModelVersion ('1.0.0.0', '6.3.164.0')
.EXAMPLE
    Check-Dependency-AXModel -AXModelName ('SYS Model', 'Foundation') -AXModelVersion ('1.0.0.0', '6.3.164.0') -ManagementUtilitiesPath 'C:\PathToDirectory' -AOSInstanceName 'MicrosoftDynamicsAX' -OutFilePath - 'C:\PathToFile\fileName.txt'    
.EXAMPLE
    Check-Dependency-Hotfix -Hotfix 'KB3151864'
.EXAMPLE
    Check-Dependency-Hotfix -Hotfix @('KB3151864', 'KB3014442') -InstallMissingHotfix -HotfixRepositoryURL 'http://artifacts.lsretail.local:8081/artifactory/webapp/#/home'
#>


function Check-Dependency-AXModel (
    [Parameter(Mandatory=$True)] [string[]] $AXModelName                                                  , 
    [Parameter(Mandatory=$True)] [string[]] $AXModelVersion                                               ,
    [string]   $ManagementUtilitiesPath = "$env:ProgramFiles\Microsoft Dynamics AX\60\ManagementUtilities",
    [string]   $AOSInstanceName                                                                           ,
    [string]   $OutFilePath             = "$ManagementUtilitiesPath\AXInstalledModels.txt"
)
{  
    <#
    .SYNOPSIS
        Validates if specified model(s) is | are available. 
    .PARAMETER AXModelName
        Mandatory. Specifies the model(s) to verify. 
        Note: The positions within the array will be used to match items in $Layer, $AXModel, and $AXModelVersion. 
        With that in mind keep them in sync.
    .PARAMETER AXModelVersion
        Manadatory. Specifies the version(s) to verify.
        Note: The positions within the array will be used to match items in $Layer, $AXModel, and $AXModelVersion. 
        With that in mind keep them in sync.
    .PARAMETER ManagementUtilitiesPath
        Optional. Specifies the path to the Microsoft Dynamics AX 2012 R3 management utilities directory.
        The default value is "$env:ProgramFiles\Microsoft Dynamics AX\60\ManagementUtilities".
    .PARAMETER AOSInstanceName
        Optional. Specifies an Application Object Server (AOS) configuration to use to 
        determine the model store database and server name. The default for AOS
        instance is the name of the "01" AOS instance on the local computer. The 
        default for name is the name of the current configuration on the specified 
        instance.
        Note: Instance Name can be found using the Microsoft Dynamics AX Configuration Utility on the Connection tab.
    .PARAMETER OutFilePath
        Optional. Specifies the path that the output from AXUtil.exe list will sent to. The default value is
        "$ManagementUtilitiesPath\AXInstalledModels.txt".
    .INPUTS
        None. You cannot pipe object to Check-Dependency-AXModel.
    .OUTPUTS
        Returns boolean value either $true, if model(s) was | were located, or $false.
    .Notes
        1. This command will need to be run on the machine hosting the Application Object Server (AOS)
        2. Unable to use Get-AXModel for this due to using Chocolatey for the install. For some reason 
           Get-AXModel will not return a value during a Chocolatey install. To compensate I am using 
           AXUtil.exe to get the model details, but this must be outputted to a file in order to get
           a return value.
    .EXAMPLE
        Check-Dependency-AXModel -AXModelName ('SYS Model', 'Foundation') -AXModelVersion ('1.0.0.0', '6.3.164.0')
    .EXAMPLE
        Check-Dependency-AXModel -AXModelName ('SYS Model', 'Foundation') -AXModelVersion ('1.0.0.0', '6.3.164.0') -ManagementUtilitiesPath 'C:\PathToDirectory' -AOSInstanceName 'MicrosoftDynamicsAX' -OutFilePath - 'C:\PathToFile\fileName.txt'
    #> 
    Write-Debug ('PARAM $AXModelName             = ' + $AXModelName            )
    Write-Debug ('PARAM $AXModelVersion          = ' + $AXModelVersion         )
    Write-Debug ('PARAM $ManagementUtilitiesPath = ' + $ManagementUtilitiesPath)
    Write-Debug ('PARAM $AOSInstanceName         = ' + $AOSInstanceName        )
    Write-Debug ('PARAM $OutFilePath             = ' + $OutFilePath            )

    cd $ManagementUtilitiesPath

    if($AOSInstanceName)
    {
        .\AXUtil.exe list /config:$AOSInstanceName -Verbose | Out-File $OutFilePath
    }
    else
    {
        .\AXUtil.exe list -Verbose | Out-File $OutFilePath
    }

    $AXModelFound     = New-Object bool[] $AXModelName.Length
    $InstalledAXModel = Get-Content $OutFilePath
    
    :nextModel for($Model = 0; $Model -ne $AXModelName.Length; $Model++)
    {
        for($line = 7; $line -ne $InstalledAXModel.Length - 2; $line++)
        {
            if(
                (Select-String -InputObject $InstalledAXModel[$line] -Pattern $AXModelName[$Model]) -and
                (Select-String -InputObject $InstalledAXModel[$line] -Pattern $AXModelVersion[$Model])
            )
            {
                Write-Debug ($InstalledAXModel[$line] + " does not contain Model: " +
                        $AXModelName[$Model] + " Version: " + $AXModelVersion[$Model])
                $AXModelFound[$Model] = $true
                continue nextModel
            }
            else
            {
                Write-Debug ($InstalledAXModel[$line] + " does not contain Model: " +
                        $AXModelName[$Model] + " Version: " + $AXModelVersion[$Model])
                $AXModelFound[$Model] = $false

                #TODO:Add option to install new ax model via [switch] $Install
            }
        }
    }
    
    if($AXModelFound.Contains($false))
    {
        return $false
    }
    else
    {
        return $true
    }
}

function Check-Dependency-Hotfix (
    [Parameter(Mandatory=$True)] [string[]] $Hotfix,
    [switch] $InstallMissingHotfix                 ,
    [string] $HotfixRepositoryURL
)
{
    <#
    .SYNOPSIS
        Validates if specified Hotfix(s) is | are available.
    .PARAMETER Hotfix
        Manadatory. Specifies the Hotfix(s) to verify.        
    .PARAMETER InstallHotfix
        Optional. Represents should program attempt to install missing Hotfix(s).
        Default value is false.
    .PARAMETER HotfixRepository
        Optional. This is a required parameter if $InstallMissingHotfix is set to $true. 
    .INPUTS
        None. You cannot pipe object to Check-Dependency-Hotfix.
    .OUTPUTS
        Returns boolean value either $true, if model(s) was | were located, or $false.        
    .EXAMPLE
        Check-Dependency-Hotfix -Hotfix 'KB3151864'
    .EXAMPLE
        Check-Dependency-Hotfix -Hotfix @('KB3151864', 'KB3014442') -InstallMissingHotfix -HotfixRepository 'http://artifacts.lsretail.local:8081/artifactory/webapp/#/home'
    #> 
    Write-Debug ('PARAM $Hotfix               = ' + $Hotfix              )
    Write-Debug ('PARAM $InstallMissingHotfix = ' + $InstallMissingHotfix)
    Write-Debug ('PARAM $HotfixRepository  = ' + $HotfixRepository       )

    if($InstallMissingHotfix -and $HotfixRepository -eq '')
    {
        Write-Debug 'Cannot call Check-Dependency-Hotfix with $InstallMissingHotfix set to $true and $HotfixRepository set to $null.'
        return $false
    }

    foreach($KB in $Hotfix)
    {
        if(-not(Get-HotFix -Id $KB -ErrorAction SilentlyContinue))
        {
            Write-Debug ("$KB was not installed in $env:COMPUTERNAME.")
             
            if($InstallMissingHotfix)
            {
                Write-Debug ("Attempting to install missing KB - $KB in $env:COMPUTERNAME")
                Start-Process -FilePath "wusa.exe" -ArgumentList "$HotfixRepository\$KB.msu /quiet /norestart" -Wait

                #Check again after attempted installation
                if(-not(Get-HotFix -Id $KB -ErrorAction SilentlyContinue))
                {
                    Write-Debug ("$KB did not install in $env:COMPUTERNAME.")
                    return $false
                }
            }
            else
            {
                return $false
            }
        }
    }
    
    return $true    
}